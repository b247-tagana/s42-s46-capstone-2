const Order = require("../models/Order");

module.exports.checkout = (data) => {
  if (data.role !== 'user') { // assuming non-admin users have the role of 'user'
    return Promise.resolve({
      message: "User must be non-admin to access this!",
    });
  }

  let newOrder = new Order({
    userId: data.userId,
    products: data.products,
    totalAmount: 0,
    purchasedOn: new Date(),
  });

  return newOrder
    .save()
    .then((order) => {
      return {
        message: "New order successfully created!",
      };
    })
    .catch((error) => {
      return {
        message: "An error occurred while saving the order.",
      };
    });
};
